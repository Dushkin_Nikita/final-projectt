//
//  ViewController.swift
//  FinalProject
//
//  Created by Nikita Dushkin on 7/15/20.
//  Copyright © 2020 Nikita Dushkin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    enum Constants {
        static let tableIdentifier = "ExpensesCell"
        static let buttonCornerRadius: CGFloat = 6
        
    }
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outcomeButton: UIButton!
    @IBOutlet weak var incomeButton: UIButton!
    
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    func setupViews() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.showsVerticalScrollIndicator = false
        incomeButton.layer.cornerRadius = Constants.buttonCornerRadius
        outcomeButton.layer.cornerRadius = Constants.buttonCornerRadius
    }
    
    // MARK: - Button actions
    @IBAction func outcomeButtonPressed(_ sender: Any) {
    
    }
    
    @IBAction func incomeButtonPressed(_ sender: Any) {
    }
    @IBAction func editActionPressed(_ sender: Any) {
        let alert = UIAlertController (title: "Введите Ваш баланс", message: "",
                                       preferredStyle: .alert)
        alert.addTextField { (textField) in
            
        }
        let okAction = UIAlertAction(title: "Добавить", style: .default) { _ in
            let textField = alert.textFields?[0]
            if let text = textField?.text {
                UserDefaults.standard.set(0, forKey: "UserTotalAmount")
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .destructive, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableIdentifier,
                                                 for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 3
//    }
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "15.01.2020"
//    }
}
